# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3

DESCRIPTION="Cross-platform library for building Telegram clients"
HOMEPAGE="https://github.com/tdlib/td"

EGIT_REPO_URI="https://github.com/tdlib/td.git"
EGIT_CLONE_TYPE="single"
EGIT_COMMIT="8d7bda00a535d1eda684c3c8802e85d69c89a14a"

LICENSE="Boost-1.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-libs/openssl
sys-libs/zlib
dev-util/gperf"
RDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
		-DCMAKE_BUILD_TYPE=Release
		-DCMAKE_INSTALL_PREFIX:PATH=/usr
	)
	cmake_src_configure
}
