# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Cross-platform library for building Telegram clients"
HOMEPAGE="https://github.com/tdlib/td"

SRC_URI="https://github.com/tdlib/td/archive/refs/tags/v${PV}.tar.gz"

LICENSE="Boost-1.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-libs/openssl
sys-libs/zlib
dev-util/gperf"
RDEPEND="${DEPEND}"

S="${WORKDIR}/td-${PV}"

src_configure() {
	local mycmakeargs=(
		-DCMAKE_BUILD_TYPE=Release
		-DCMAKE_INSTALL_PREFIX:PATH=/usr
	)
	cmake_src_configure
}
