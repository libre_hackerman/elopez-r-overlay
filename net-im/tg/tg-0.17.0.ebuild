# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..10} )
inherit distutils-r1

DESCRIPTION="terminal telegram client that really works"
HOMEPAGE="https://github.com/paul-nameless/tg"
SRC_URI="https://github.com/paul-nameless/${PN}/archive/refs/tags/v${PV}.tar.gz"

LICENSE="Unlicense"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}
dev-python/python-telegram"
BDEPEND=""
