# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Offline copy of cppreference.com. Fork by PeterFeicht"
HOMEPAGE="https://github.com/PeterFeicht/cppreference-doc"
SRC_URI="https://github.com/PeterFeicht/cppreference-doc/releases/download/v${PV}/html-book-${PV}.tar.xz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="media-fonts/dejavu"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_install()
{
	rm reference/common/DejaVuSansMonoCondensed60.ttf
	rm reference/common/DejaVuSansMonoCondensed75.ttf

	dodoc -r reference/common
	dodoc -r reference/en
}
