# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Offline copy of cppreference.com with unnecessary UI elements stripped out"
HOMEPAGE="https://en.cppreference.com/w/Cppreference:Archives"
SRC_URI="https://upload.cppreference.com/mwiki/images/1/16/html_book_${PV}.tar.xz"

LICENSE="CC-BY-SA-3.0"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="media-fonts/dejavu"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_install()
{
	rm reference/common/DejaVuSansMonoCondensed60.ttf
	rm reference/common/DejaVuSansMonoCondensed75.ttf

	dodoc -r reference/common
	dodoc -r reference/en
}
